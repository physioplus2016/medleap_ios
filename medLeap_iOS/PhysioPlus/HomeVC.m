//
//  HomeVC.m
//  PhysioPlus
//
//  Created by Aditya Patil on 9/23/16.
//  Copyright © 2016 MedHacks. All rights reserved.
//

#import "HomeVC.h"
#import "ExcerciseDetailVC.h"
#import <XCDYouTubeKit/XCDYouTubeKit.h>
#import "MBProgressHUD.h"

@interface HomeVC ()

@end

@implementation HomeVC {
    NSMutableArray *arrExcercise;
    NSMutableArray *arrExcerciseID;
    NSMutableArray *arrProgressDate;
    NSMutableArray *arrProgressVal;
    int exerciseId;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    // Data initialization
    NSError *error = nil;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:self.data options:kNilOptions error:&error];
    
    if (error != nil) {
        NSLog(@"Error parsing JSON.");
    }
    else {
        self.lblDocName.text = [[jsonDict objectForKey:@"doctor"] objectForKey:@"name"];
        [self.btnPhone setTitle:[[jsonDict objectForKey:@"doctor"] objectForKey:@"phone"] forState:UIControlStateNormal];
        [self.btnEmail setTitle:[[jsonDict objectForKey:@"doctor"] objectForKey:@"email"] forState:UIControlStateNormal];
        
        // Array Exercise
        arrExcercise = [[NSMutableArray alloc] init];
        arrExcerciseID = [[NSMutableArray alloc] init];
        NSArray *arrExerciseList = [[NSArray alloc]initWithArray:[jsonDict objectForKey:@"exerciseList"]];
        for (int i=0; i<arrExerciseList.count; i++) {
            [arrExcercise addObject:[arrExerciseList[i] objectForKey:@"name"]];
            [arrExcerciseID addObject:[arrExerciseList[i] objectForKey:@"exerciseId"]];
        }
        
        //Daily Average
        NSMutableDictionary *dictDailyAvg = [[NSMutableDictionary alloc] init];
        arrProgressVal = [[NSMutableArray alloc] init];
        
        NSArray *arrTempDailyProgress = [[NSArray alloc]initWithArray:[jsonDict objectForKey:@"dailyProgressList"]];
        
        for (int i=0; i<arrTempDailyProgress.count; i++) {
            
            if (dictDailyAvg[[self formatDate:[arrTempDailyProgress[i] objectForKey:@"date"]]]) {
                NSString *strTemp = [dictDailyAvg objectForKey:[self formatDate:[arrTempDailyProgress[i] objectForKey:@"date"]]];
                [dictDailyAvg setObject:[strTemp stringByAppendingString:[NSString stringWithFormat:@"+%@", [arrTempDailyProgress[i] objectForKey:@"progressIndex"]]] forKey:[self formatDate:[arrTempDailyProgress[i] objectForKey:@"date"]]];
            }
            else {
                [dictDailyAvg setObject:[NSString stringWithFormat:@"%@", [arrTempDailyProgress[i] objectForKey:@"progressIndex"]] forKey:[self formatDate:[arrTempDailyProgress[i] objectForKey:@"date"]]];
            }
        }
        
        NSArray *arrKeysTemp = [dictDailyAvg allKeys];
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"self" ascending:YES];
        NSArray *descriptors = [NSArray arrayWithObject: descriptor];
        NSArray *arrKeys = [arrKeysTemp sortedArrayUsingDescriptors:descriptors];
        arrProgressDate = [[NSMutableArray alloc] initWithArray:arrKeys];
        
        for(int i=0; i<arrKeys.count; i++) {
            NSUInteger numberOfOccurrences = [[[dictDailyAvg objectForKey:arrKeys[i]] componentsSeparatedByString:@"+"] count];
            if(numberOfOccurrences>0) {
                NSString *strExpression = [NSString stringWithFormat:@"((%@)/%lu)*100", [dictDailyAvg objectForKey:arrKeys[i]], (unsigned long)numberOfOccurrences];
                NSExpression *e = [NSExpression expressionWithFormat:strExpression];
                NSNumber *result = [e expressionValueWithObject:nil context:nil];
                [arrProgressVal addObject:result];
            }
        }
    }
    
    // self.view background
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"innerBG"]];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = self.view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:blurEffectView];
    
    // Navigation controller
    [[self navigationController] setNavigationBarHidden:NO];
    [[self navigationItem] setTitle:@"Exercises"];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.barTintColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:20.0f]}];
    self.navigationController.navigationBar.translucent = YES;
    
    //Excercise table
    self.tblExercise.backgroundColor = [UIColor clearColor];
    [self.tblExercise setDelegate:self];
    [self.tblExercise setDataSource:self];
    self.tblExercise.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // Right bar button
    UIBarButtonItem *button = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(rightBarButtonAction)];
    self.navigationItem.rightBarButtonItem = button;
    
    //Lower view (doctor details)
    self.lblDocName.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20.0f];
    self.lblDocName.textColor = [UIColor whiteColor];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        // Do something...
        dispatch_async(dispatch_get_main_queue(), ^{
            [self drawChart];
            [self performSelector:@selector(endHUD) withObject:NULL afterDelay:1.0];
        });
    });
    [self.tblExercise reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrExcercise.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];

    UIButton *btnAccessory = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnAccessory setTitle:@"btn" forState:UIControlStateNormal];
    [btnAccessory addTarget:self action:@selector(btnAccessoryTapped:event:) forControlEvents:UIControlEventTouchUpInside];
    [btnAccessory setFrame:CGRectMake(0, 0, 28.0, 28.0)];
    UIImageView *imgViewAccessory = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"imgVideo"]];
    [imgViewAccessory setFrame:CGRectMake(0, 0, 28.0, 28.0)];
    [btnAccessory setImage:imgViewAccessory.image forState:UIControlStateNormal];
    cell.accessoryView = btnAccessory;

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20.0f];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.text = arrExcercise[indexPath.row];
    return cell;
}

- (void)btnAccessoryTapped:(id)sender event:(id)event{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tblExercise];
    NSIndexPath *indexPath = [self.tblExercise indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil){
        [self tableView: self.tblExercise accessoryButtonTappedForRowWithIndexPath: indexPath];
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {

//    XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:@"9bZkp7q19f0"];
//    [self presentViewController:videoPlayerViewController animated:YES completion:nil];
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Error! Video not found."
                                 message:@"Please try again later"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:nil];
    
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ExcerciseDetailVC *objExcerciseDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EDVC"];
    [[objExcerciseDetailVC navigationItem] setTitle:arrExcercise[indexPath.row]];
    objExcerciseDetailVC.data = self.data;
    objExcerciseDetailVC.exerciseId = arrExcerciseID[indexPath.row];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController pushViewController:objExcerciseDetailVC animated:YES];
}

#pragma mark - Chart

- (void)drawChart
{
    
    AmMobileChartView *chartView = [[AmMobileChartView alloc] initWithFrame:self.viewChartView.bounds];
    [self.viewChartView addSubview:chartView];
    
    AmStockChart *excerciseGraph = [[AmStockChart alloc] init];
    excerciseGraph.type = @"stock";
    excerciseGraph.pathToImages = @"amcharts/images/";
    excerciseGraph.dataDateFormat = @"YYYY-MM-DD";
    AmDataSet *dataSet = [[AmDataSet alloc] init];
    
    NSMutableArray *dataProvider = [[NSMutableArray alloc] init];
    for(int i=0; i<arrProgressDate.count; i++) {
        [dataProvider addObject:@{@"date": arrProgressDate[i], @"val": arrProgressVal[i]}];
    }
    
    dataSet.dataProvider = dataProvider;
    dataSet.fieldMappings = [@[@{@"fromField" : @"val", @"toField" : @"value"}] mutableCopy];
    dataSet.categoryField = @"date";
    
    excerciseGraph.dataSets = [@[dataSet] mutableCopy];
    
    AmStockPanel *panel = [[AmStockPanel alloc] init];
    panel.showCategoryAxis = @(true);
    
    AmStockGraph *stockGraph = [[AmStockGraph alloc] init];
    stockGraph.uid = @"graph1";
    stockGraph.valueField = @"value";
    stockGraph.type = @"line";
    stockGraph.title = @"Exercise Graph";
    stockGraph.fillAlphas = @(0.0);
    stockGraph.bullet = @"round";
    
    AmChartCursor *cursor = [[AmChartCursor alloc] init];
    cursor.selectWithoutZooming = @(NO);
    cursor.cursorPosition = @"mouse";
    cursor.zoomable = @(NO);
    panel.chartCursor = cursor;
    
    panel.stockGraphs = [@[stockGraph] mutableCopy];
    excerciseGraph.panels = [@[panel] mutableCopy];
    
    excerciseGraph.panelsSettings.startDuration = @(0);
    excerciseGraph.categoryAxesSettings.dashLength = @(5);
    excerciseGraph.valueAxesSettings.dashLength = @(5);
    excerciseGraph.chartScrollbarSettings.enabled = @(NO);
    excerciseGraph.valueAxesSettings.showFirstLabel = @(1);
    excerciseGraph.valueAxesSettings.showLastLabel = @(1);
    
    [chartView setChart:excerciseGraph];
    [chartView drawChart];
}

#pragma mark - Action methods

- (IBAction)btnActionPhone:(UIButton *)sender {
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:self.btnPhone.titleLabel.text];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

- (IBAction)btnActionEmail:(UIButton *)sender {
    NSString *url = [NSString stringWithFormat:@"mailto:%@", self.btnEmail.titleLabel.text];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
}

#pragma mark - Helper methods

- (NSString *) formatDate: (NSString *)dateValue {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *myDate = [dateFormatter dateFromString:dateValue];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *newlyFormattedDateString = [dateFormatter stringFromDate:myDate];
    return newlyFormattedDateString;
}

- (void) endHUD {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void) rightBarButtonAction {
    //    [self GETMethod];
}

/*
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
