//
//  HomeVC.h
//  PhysioPlus
//
//  Created by Aditya Patil on 9/23/16.
//  Copyright © 2016 MedHacks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <AmChartsMobile/AmCharts.h>

@interface HomeVC : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UIView *viewChartView;
@property (nonatomic, strong) IBOutlet UIView *viewUpperView;
@property (nonatomic, strong) IBOutlet UIView *viewLowerView;

@property (nonatomic, strong) IBOutlet UITableView *tblExercise;
@property (nonatomic, strong) IBOutlet UILabel *lblDocName;
@property NSData *data;

@property (strong, nonatomic) IBOutlet UIButton *btnPhone;
@property (strong, nonatomic) IBOutlet UIButton *btnEmail;
- (IBAction)btnActionPhone:(UIButton *)sender;
- (IBAction)btnActionEmail:(UIButton *)sender;

@end
