//
//  ExcerciseDetailVC.h
//  PhysioPlus
//
//  Created by Aditya Patil on 9/24/16.
//  Copyright © 2016 MedHacks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AmChartsMobile/AmCharts.h>


@interface ExcerciseDetailVC : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UIView *viewChartView;
@property (strong, nonatomic) IBOutlet UIView *viewTableView;

@property (nonatomic, strong) IBOutlet UITableView *tblExerciseDates;

@property NSData *data;
@property NSNumber *exerciseId;

@end
