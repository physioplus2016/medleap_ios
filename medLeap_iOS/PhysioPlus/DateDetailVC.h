//
//  DateDetailVC.h
//  PhysioPlus
//
//  Created by Aditya Patil on 9/24/16.
//  Copyright © 2016 MedHacks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KNCirclePercentView.h"

@interface DateDetailVC : UIViewController

@property (strong, nonatomic) IBOutlet UIView *viewUserCommentView;

@property (strong, nonatomic) IBOutlet UILabel *lblProgress;
@property (weak, nonatomic) IBOutlet KNCirclePercentView *viewProgressView;

@property (strong, nonatomic) IBOutlet UITextView *txtUserComment;

@property (strong, nonatomic) NSString *strUserComment;
@property (strong, nonatomic) NSString *strDocComment;
@property NSNumber *intProgress;

@end
