//
//  ViewController.h
//  PhysioPlus
//
//  Created by Aditya Patil on 9/23/16.
//  Copyright © 2016 MedHacks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *txtUsername;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UIButton *btnLogin;

- (IBAction)btnActionLogin:(UIButton *)sender;

@end

