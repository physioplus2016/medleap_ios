//
//  ViewController.m
//  PhysioPlus
//
//  Created by Aditya Patil on 9/23/16.
//  Copyright © 2016 MedHacks. All rights reserved.
//

#import "ViewController.h"
#import "HomeVC.h"
#import "MBProgressHUD.h"

@interface ViewController ()

@end

@implementation ViewController {
    NSData *GETData;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = self.view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:blurEffectView];

    //Username textfield
    self.txtUsername.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.txtUsername.layer.borderWidth=1.0;
    self.txtUsername.layer.cornerRadius = 7.0;
    self.txtUsername.clipsToBounds = YES;
    
    //Password textfield
    self.txtPassword.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.txtPassword.layer.borderWidth=1.0;
    self.txtPassword.layer.cornerRadius = 7.0;
    self.txtPassword.clipsToBounds = YES;

    //Login button
    self.btnLogin.layer.cornerRadius = 7.0;
    self.btnLogin.clipsToBounds = YES;

    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    //TODO: Delete following code before building...!!!
    self.txtUsername.text = @"patient123";
    self.txtPassword.text = @"patient123";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnActionLogin:(UIButton *)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self GETMethod];
}

- (void) endHUD {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - Get

-(void)GETMethod
{
    NSString *strURL = @"http://localhost:8080/physio/api/patients/1";
    NSURLSession *session = [NSURLSession sharedSession];
    
    [[session dataTaskWithURL:[NSURL URLWithString:strURL] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //            self.json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if (error) {
                [self performSelector:@selector(endHUD) withObject:NULL afterDelay:1.0];
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Error! Server not responding"
                                             message:@"Please try again later"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* okButton = [UIAlertAction
                                            actionWithTitle:@"OK"
                                            style:UIAlertActionStyleDefault
                                            handler:nil];
                
                [alert addAction:okButton];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else {
                GETData = data;
                [self performSelector:@selector(endHUD) withObject:NULL afterDelay:1.0];
                HomeVC *objHomeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HVC"];
                objHomeVC.data = GETData;
                self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:nil action:nil];
                [self.navigationController pushViewController:objHomeVC animated:YES];
            }
            
        });
    }] resume];
}

@end
