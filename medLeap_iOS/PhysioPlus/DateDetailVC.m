//
//  DateDetailVC.m
//  PhysioPlus
//
//  Created by Aditya Patil on 9/24/16.
//  Copyright © 2016 MedHacks. All rights reserved.
//

#import "DateDetailVC.h"

@interface DateDetailVC ()

@end

@implementation DateDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // self.view background
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"innerBG"]];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = self.view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:blurEffectView];

    if ([self.strDocComment isEqualToString:@"No Doctors comment to display."]) {
        self.txtUserComment.text = [NSString stringWithFormat:@"\nMe: %@\n\n%@", self.strUserComment, self.strDocComment];
    }
    else {
        self.txtUserComment.text = [NSString stringWithFormat:@"\nMe: %@\n\nDoctor: %@", self.strUserComment, self.strDocComment];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    [UIView animateWithDuration:0.5 animations:^{
        self.lblProgress.alpha = 0.0;
    } completion:^(BOOL finished) {
        if(finished)
            [self performSelector:@selector(showProgress) withObject:NULL afterDelay:0.0];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Progress Bar

- (void)showProgress {
    
    float G = [self.intProgress floatValue] / 100.0;
    float R = (100.0 - [self.intProgress floatValue]) / 100.0;
    float B = 0.0;
    
    UIColor *strokeColor = [UIColor colorWithRed:R green:G blue:B alpha:1.0];
    
    [self.viewProgressView drawCircleWithPercent:[self.intProgress floatValue]
                                               duration:2
                                              lineWidth:10
                                              clockwise:YES
                                                lineCap:kCALineCapRound
                                              fillColor:[UIColor clearColor]
                                            strokeColor:strokeColor
                                         animatedColors:nil];
    self.viewProgressView.percentLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:45.0f];
    self.viewProgressView.percentLabel.textColor = [UIColor whiteColor];
    [self.viewProgressView startAnimation];
}

@end
