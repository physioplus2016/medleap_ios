//
//  ExcerciseDetailVC.m
//  PhysioPlus
//
//  Created by Aditya Patil on 9/24/16.
//  Copyright © 2016 MedHacks. All rights reserved.
//

#import "ExcerciseDetailVC.h"
#import "DateDetailVC.h"
#import "MBProgressHUD.h"

@interface ExcerciseDetailVC ()

@end

@implementation ExcerciseDetailVC {
    NSArray *dailyProgressList;
    NSMutableArray *arrDate;
    NSMutableArray *arrVal;
    NSMutableArray *arrUserComm;
    NSMutableArray *arrDocComm;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Array initialization
    arrDate = [[NSMutableArray alloc] init];
    arrVal = [[NSMutableArray alloc] init];
    arrUserComm = [[NSMutableArray alloc] init];
    arrDocComm = [[NSMutableArray alloc] init];
    
    //Data initialization
    NSError *error = nil;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:self.data options:kNilOptions error:&error];
    
    if (error != nil) {
        NSLog(@"Error parsing JSON.");
    }
    else {
        dailyProgressList = [[NSArray alloc]initWithArray:[jsonDict objectForKey:@"dailyProgressList"]];
        for (int i=0; i<dailyProgressList.count; i++) {
            if (self.exerciseId == [dailyProgressList[i] objectForKey:@"exerciseId"]) {
                [arrDate addObject:[dailyProgressList[i] objectForKey:@"date"]];
                [arrVal addObject:@([[dailyProgressList[i] objectForKey:@"progressIndex"] doubleValue] * 100.0)];
                if([dailyProgressList[i] objectForKey:@"patientComment"] == NULL)
                    [arrUserComm addObject:@"null"];
                else
                    [arrUserComm addObject:[dailyProgressList[i] objectForKey:@"patientComment"]];
                if([dailyProgressList[i] objectForKey:@"doctorComment"] == NULL)
                    [arrDocComm addObject:@"null"];
                else
                    [arrDocComm addObject:[dailyProgressList[i] objectForKey:@"doctorComment"]];
            }
        }
    }
    
    // self.view background
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"innerBG"]];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = self.view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:blurEffectView];
    
    // table view
    self.tblExerciseDates.backgroundColor = [UIColor clearColor];
    if (arrDate.count != 0) {
        [self.tblExerciseDates setDelegate:self];
        [self.tblExerciseDates setDataSource:self];
        self.tblExerciseDates.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    if (arrDate.count == 0) {
        UIView *superview = [[UIView alloc] initWithFrame:self.view.bounds];
        superview.backgroundColor = [UIColor clearColor];
        superview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"innerBG"]];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.view.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [superview addSubview:blurEffectView];

        UILabel *lblNoRecords = [[UILabel alloc] initWithFrame:superview.bounds];
        lblNoRecords.text = @"No records found.";
        lblNoRecords.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0f];
        lblNoRecords.textColor = [UIColor whiteColor];
        lblNoRecords.textAlignment = NSTextAlignmentCenter;
        [superview addSubview:lblNoRecords];
        [self.view addSubview:superview];
    }
    else {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            // Do something...
            dispatch_async(dispatch_get_main_queue(), ^{
                [self drawChart];
                [self performSelector:@selector(endHUD) withObject:NULL afterDelay:1.0];
            });
        });
        [self.tblExerciseDates reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrDate.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
    
    if(![arrDocComm[indexPath.row] isEqualToString:@"null"] && ![arrDocComm[indexPath.row] isEqualToString:@"NULL"] && arrDocComm[indexPath.row] != NULL)
        cell.accessoryView = [self setCellAccessory:cell];
    else
        cell.accessoryView = NULL;
    
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.text = [self formatDate:arrDate[indexPath.row]];
    return cell;
}

- (UILabel *) setCellAccessory: (UITableViewCell *)cell{
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60.0, cell.frame.size.height)];
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:8.0f];
    titleLabel.text = @"Doctors comment available";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.numberOfLines = 3;
    titleLabel.textColor = [UIColor colorWithRed:0.3882 green:0.7255 blue:0.4980 alpha:1.0];
    return titleLabel;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DateDetailVC *objDateDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DDVC"];
    objDateDetailVC.title =[self formatDate:arrDate[indexPath.row]];
    objDateDetailVC.strUserComment = [self setComment:arrUserComm[indexPath.row] :FALSE];
    objDateDetailVC.strDocComment = [self setComment:arrDocComm[indexPath.row] :TRUE];
    objDateDetailVC.intProgress = @([[dailyProgressList[indexPath.row] objectForKey:@"progressIndex"] doubleValue] * 100.0);
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController pushViewController:objDateDetailVC animated:YES];
}

#pragma mark - Chart

- (void)drawChart
{
    
    AmMobileChartView *chartView = [[AmMobileChartView alloc] initWithFrame:self.viewChartView.bounds];
    [self.viewChartView addSubview:chartView];
    
    AmStockChart *excerciseGraph = [[AmStockChart alloc] init];
    excerciseGraph.type = @"stock";
    excerciseGraph.pathToImages = @"amcharts/images/";
    excerciseGraph.dataDateFormat = @"YYYY-MM-DD";
    AmDataSet *dataSet = [[AmDataSet alloc] init];
    
    NSMutableArray *dataProvider = [[NSMutableArray alloc] init];
    for(int i=0; i<arrDate.count; i++) {
        [dataProvider addObject:@{@"date": arrDate[i], @"val": arrVal[i]}];
    }
    
    dataSet.dataProvider = dataProvider;
    dataSet.fieldMappings = [@[@{@"fromField" : @"val", @"toField" : @"value"}] mutableCopy];
    dataSet.categoryField = @"date";
    
    excerciseGraph.dataSets = [@[dataSet] mutableCopy];
    
    AmStockPanel *panel = [[AmStockPanel alloc] init];
    //panel.legend = [[AmStockLegend alloc] init];
    panel.showCategoryAxis = @(true);
    
    AmStockGraph *stockGraph = [[AmStockGraph alloc] init];
    stockGraph.uid = @"graph1";
    stockGraph.valueField = @"value";
    stockGraph.type = @"line";
    stockGraph.title = @"Exercise Graph";
    stockGraph.fillAlphas = @(0.0);
    stockGraph.bullet = @"round";
    
    AmChartCursor *cursor = [[AmChartCursor alloc] init];
    cursor.selectWithoutZooming = @(NO);
    cursor.cursorPosition = @"mouse";
    cursor.zoomable = @(NO);
    panel.chartCursor = cursor;
    
    panel.stockGraphs = [@[stockGraph] mutableCopy];
    excerciseGraph.panels = [@[panel] mutableCopy];
    
    excerciseGraph.panelsSettings.startDuration = @(0);
    excerciseGraph.categoryAxesSettings.dashLength = @(5);
    excerciseGraph.valueAxesSettings.dashLength = @(5);
    excerciseGraph.chartScrollbarSettings.enabled = @(NO);
    excerciseGraph.valueAxesSettings.showFirstLabel = @(1);
    excerciseGraph.valueAxesSettings.showLastLabel = @(1);
    
    [chartView setChart:excerciseGraph];
    [chartView drawChart];
}

#pragma mark - helper methods
- (NSString *) setComment: (NSString *)strComm :(BOOL)isDoc{
    
    NSString *strCommDisplay = @"";
    if ([strComm isEqualToString:@"null"] || [strComm isEqualToString:@"NULL"]) {
        if(isDoc)
            strCommDisplay = @"No Doctors comment to display.";
        else
            strCommDisplay = @"No comment to display.";
    }
    else {
        strCommDisplay = strComm;
    }
    return strCommDisplay;
}

- (NSString *) formatDate: (NSString *)dateValue {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *myDate = [dateFormatter dateFromString:dateValue];
    [dateFormatter setDateFormat:@"EEEE, dd MMMM yyyy"];
    NSString *newlyFormattedDateString = [dateFormatter stringFromDate:myDate];
    return newlyFormattedDateString;
}

- (void) endHUD {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
