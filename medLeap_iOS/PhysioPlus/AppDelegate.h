//
//  AppDelegate.h
//  PhysioPlus
//
//  Created by Aditya Patil on 9/23/16.
//  Copyright © 2016 MedHacks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

